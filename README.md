# TestStand-Python-MatLab

To run the SPS code build in MATLAB automatically from TESTSTAND so that it can be interfaced with ADCS.

Install MATLAB Engine API for Python 
# Step 1
To start the MATLAB® engine within a Python® session, you first must install the engine API as a Python package. MATLAB provides a standard Python setup.py file for building and installing the engine using the distutils module. You can use the same setup.py commands to build and install the engine on Windows®, Mac, or Linux® systems.

Each MATLAB release has a Python setup.py package. When you use the package, it runs the specified MATLAB version. To switch between MATLAB versions, you need to switch between the Python packages

# Step 2
Verify Your Configuration
Before you install, verify your Python and MATLAB configurations.

Check that your system has a supported version of Python and MATLAB R2014b or later. For more information, see Versions of Python Supported by MATLAB Products by Release.

To check that Python is installed on your system, run Python at the operating system prompt.

Add the folder that contains the Python interpreter to your path, if it is not already there.

Find the path to the MATLAB folder. Start MATLAB and type matlabroot in the command window. Copy the path returned by matlabroot.
# Step 3: Install the Engine API

To install the engine API, choose one of the following. You must call this python install command in the specified folder.

At a Windows operating system prompt (you might need administrator privileges to execute these commands) —
```
cd "matlabroot\extern\engines\python"
python setup.py install
```

At a macOS or Linux operating system prompt (you might need administrator privileges to execute these commands) —
```
cd "matlabroot/extern/engines/python"
python setup.py install
```

At the MATLAB command prompt —
```
cd (fullfile(matlabroot,'extern','engines','python'))
system('python setup.py install')
```

Use one of the nondefault options described in Install MATLAB Engine API for Python in Nondefault Locations.

# Step 4: Start MATLAB Engine

Start Python, import the module, and start the MATLAB engine:
```
import matlab.engine
eng = matlab.engine.start_matlab()
```
Install Python Engine for Multiple MATLAB Versions
You can specify a MATLAB version to run from a Python script by installing the MATLAB Python packages to a version-specific locations. For example, suppose that you want to call either MATLAB R2019a or R2019b from a Python version 3.6 script.

From the Windows system prompt, install the R2019a package in a subfolder named matlab19aPy36:
```
cd "c:\Program Files\MATLAB\R2019a\extern\engines\python" 
python setup.py install --prefix="c:\work\matlab19aPy36"
```
Install the R2019b package in a matlab19bPy36 subfolder:
```
cd "c:\Program Files\MATLAB\R2019b\extern\engines\python" 
python setup.py install --prefix="c:\work\matlab19bPy36"
```

From a Linux system prompt:
```
cd "/usr/local/MATLAB/R2019a/bin/matlab/extern/engines/python"
python setup.py install --prefix="/local/work/matlab19aPy36"
cd "/usr/local/MATLAB/R2019b/bin/matlab/extern/engines/python"
python setup.py install --prefix="/local/work/matlab19bPy36"
```
From a Mac Terminal:
```
cd "/Applications/MATLAB_R2019a.app/extern/engines/python"
python setup.py install --prefix="/local/work/matlab19aPy36"
cd "/Applications/MATLAB_R2019b.app/extern/engines/python"
python setup.py install --prefix="/local/work/matlab19bPy36"
```
Start Specific MATLAB Engine Version
To start a specific version of the MATLAB engine, set the PYTHONPATH environment variable to the location of the package. This code assumes you used the setup shown in the previous section. To set PYTHONPATH on Windows to call MATLAB R2019b, type:
```
sys.path.append("c:\work\matlab19bPy36")
```
On Linux or Mac:
```
sys.path.append("/local/work/matlab19bPy36")
```
To check which version of MATLAB was imported, in Python type:
```
import matlab
print(matlab.__file__)
```
Troubleshooting MATLAB Engine API for Python Installation
Make sure that your MATLAB release supports your Python version. See Versions of Python Supported by MATLAB Products by Release.

You must run the Python install command from the specified MATLAB folder. See Install the Engine API.
```
python setup.py install
```
Make sure that you have administrator privileges to execute the install command from the operating system prompt. On Windows, open the command prompt with the Run as administrator option.

The installer installs the engine in the default Python folder. To use non-default location, see Install MATLAB Engine API for Python in Nondefault Locations.

If you installed the package in a non-default folder, make sure to set the PYTHONPATH environment variable. For example, suppose that you used this installation command:
```
python setup.py install --prefix=" matlab19bPy36"
```
In Python, update PYTHONPATH with this command:
```
sys.path.append("matlab19bPy36")
```

# To run a MATALB exectable file and to test SPS realtime is working.
Git clone from this link.
```
https://github.com/omkarbhoite25/Miniature-Student-Satellite.git
```

# To create a MATLAB executable use the folloing command in the command window, and then select the Application Complier and then add the necessary files to create .exe file.
```
deploytool
```

# Procedure 
TestStand file named Squence_File will run a process when the SPS realtime will start execting. In this code a popup meesage will be displayed but it can be modified/confiured to run the ADCs files. 
Merge the code in the files Sequence_file.seq & RUN_MATLAB_exectable_from_TestStand.seq to the process simultaneously.
In order to test the execution separate files were created.
Make sure that all the files are in a same folde and path assigned are correct.

# Tips to resolve error.

1. Make sure that the code written in python script is defined as a function to run the MATLAB via TESTSTAND.\\
2. The function accessed from MATLAB is also named to the .m file of the MATLAB. (Need to check)\\
3. Always unload the modules prior to RUN the program in the teststand. (Shortcut Ctrl+Shift+U) \\
4. Always make sure that the files you're running are in the same folder.\\
5. Put the function name the same as the script name.\\
6. Define a function in python script prior to importing the file in the teststand, or else it will start executing the files continuously.



